#pragma once
#include "Vertex.h"
#include <algorithm>

class Triangle
{
public:
	Vertex v1, v2, v3;
	float depth;
	Triangle(Vertex v1, Vertex v2, Vertex v3): v1(v1), v2(v2), v3(v3)
	{
		depth = std::max<float>(std::max<float>(v1.matrix[2][0], v2.matrix[2][0]), v3.matrix[2][0]);
	}
	~Triangle(){}
};
