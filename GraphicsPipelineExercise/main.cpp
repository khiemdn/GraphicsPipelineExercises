#include "Vertex.h"
#include "Cube.h"
#include <thread>
#include "Camera.h"
#include <windows.h>
#include <gdiplus.h>
#include "Texture.h"
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")

#define WIDTH 640
#define HEIGHT 480

using namespace std;

Rasterizer* r;
Camera camera;
Matrix4x4<float> ortho = OrthographicMatrix(-10, 10, 10, -10, -10, 10);
Matrix4x4<float> viewport = ViewportTransform(0, 0, WIDTH, HEIGHT, -1, 1);
Matrix4x4<float> view = camera.LookAt(Vertex(-3, -3, 3), Vertex(0, 0, 0), Vec3<float>(0, 0, 0));
Cube model(5, 5, 5);
int i = 0;

VOID draw(HDC hdc)
{
	model.ClearTransform();
	model.ApplyTransformMatrix(CreateYRotation(i + 13));
	model.ApplyTransformMatrix(CreateXRotation(2 + i));
	model.ApplyTransformMatrix(CreateZRotation(1 - i));
//	model.ApplyTransformMatrix(CreateTranslation(3, 3, 0));
	model.ApplyTransformMatrix(view);
	model.ApplyTransformMatrix(ortho);
	model.ApplyTransformMatrix(viewport);
	
	vector<Triangle> vertices  = model.GetVerticesData();
	r->Rasterize(vertices, hdc);
}

VOID OnPaint(HDC hdc)
{
	Graphics graphics(hdc);
	Pen      pen(Color(255, 0, 0, 255));
	draw(hdc);
}

VOID OnEraseBG(HDC hdc)
{
	RECT rect;
	HBRUSH hBr;
	SetRect(&rect, 0, 0, WIDTH, HEIGHT);
	hBr = CreateSolidBrush(RGB(0, 0, 0));
	FillRect(hdc, &rect, hBr);
}

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, PSTR, INT iCmdShow)
{
	HWND                hWnd;
	MSG                 msg;
	WNDCLASS            wndClass;
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;

	// Initialize GDI+.
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = TEXT("GraphicsPipeline");

	RegisterClass(&wndClass);
	hWnd = CreateWindow(
		TEXT("GraphicsPipeline"),	// window class name
		TEXT("Graphics Pipeline"),	// window caption
		WS_OVERLAPPEDWINDOW,		// window style
		0,							// initial x position
		0,							// initial y position
		WIDTH,						// initial x size
		HEIGHT,						// initial y size
		NULL,						// parent window handle
		NULL,						// window menu handle
		hInstance,					// program instance handle
		NULL);						// creation parameters
	SetTimer(hWnd, 1, 10, NULL);
	ShowWindow(hWnd, iCmdShow);
//	UpdateWindow(hWnd);

	Bitmap* bmp = new Bitmap(L"face.png");
	Texture tex(bmp);
	r = new Rasterizer(WIDTH, HEIGHT, &tex);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	GdiplusShutdown(gdiplusToken);
	return msg.wParam;
}  // WinMain

LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{
	HDC          hdc;
	PAINTSTRUCT  ps;

	switch (message)
	{
	case WM_CREATE:
		return 0;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		OnPaint(hdc);
		EndPaint(hWnd, &ps);
		return 0;
	case WM_ERASEBKGND:
		return 1;
	case WM_TIMER:
		i = ++i % 360;
		InvalidateRect(hWnd, NULL, TRUE);
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
} // WndProc