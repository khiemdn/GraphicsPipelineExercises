#pragma once

template <class T>
struct Vec1
{
	int size;
	T data[1];

	T& operator[](int length)
	{
		return data[length];
	}

	const T& operator[](int length) const
	{
		return data[length];
	}
};

template <class T>
struct Vec2
{
	int size;
	T data[2];
	Vec2(){}
	Vec2(T t1, T t2)
	{
		data[0] = t1;
		data[1] = t2;
	}
	T& operator[](int length)
	{
		return data[length];
	}
	const T& operator[](int length) const
	{
		return data[length];
	}
	Vec2<T> operator+(const Vec2<T>& other)
	{
		Vec2<T> result;
		result[0] = data[0] + other[0];
		result[1] = data[1] + other[1];
		return result;
	}
	Vec2<T> operator-(const Vec2<T>& other)
	{
		Vec2<T> result;
		result[0] = data[0] - other[0];
		result[1] = data[1] - other[1];
		return result;
	}
	Vec2<T> operator*(const float& k)
	{
		Vec2<T> result;
		result[0] = k*data[0];
		result[1] = k*data[1];
		return result;
	}
	bool operator==(const Vec2<T>& other)
	{
		return data[0] == other[0] && data[1] == other[1];
	}
	bool operator==(const T& other)
	{
		return data[0] == other && data[1] == other;
	}
	bool operator!=(const T& other)
	{
		return data[0] != other && data[1] != other;
	}
};

template <class T>
struct Vec3
{
	int size;
	T data[3];

	Vec3(){}


	Vec3(T x, T y, T z)
	{
		data[0] = x;
		data[1] = y;
		data[2] = z;
	}

	T& operator[](int length)
	{
		return data[length];
	}

	void normalize()
	{
		const float length = sqrt(data[0] * data[0] + data[1] * data[1] + data[2] * data[2]);
		data[0] = data[0] / length;
		data[1] = data[1] / length;
		data[2] = data[2] / length;
	}
};

template <class T>
struct Vec4
{
	int size;
	T data[4];

	T& operator[](int length)
	{
		return data[length];
	}

	const T& operator[](int length) const
	{
		return data[length];
	}
};
