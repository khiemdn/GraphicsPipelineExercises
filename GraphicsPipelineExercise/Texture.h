#pragma once
#include <gdiplus.h>
#include "Vertex.h"

class Texture
{
public:
	Gdiplus::Bitmap* src;
	int width, height;
	Texture(Gdiplus::Bitmap* bmp)
	{
		src = bmp;
		width = src->GetWidth();
		height = src->GetHeight();
	}

	~Texture() {}
	MyColor GetPixel(float texX, float texY)
	{
		int pixelX = texX * width;
		int pixelY = texY * width;
		pixelX %= width;
		pixelY %= height;
		Gdiplus::Color pColor;
		src->GetPixel(pixelX, pixelY, &pColor);
		MyColor color(pColor.GetR(), pColor.GetG(), pColor.GetB());
		return color;
	}
};
