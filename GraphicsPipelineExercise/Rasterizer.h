#pragma once

#include <vector>
#include "Vertex.h"
#include "Triangle.h"
#include <windows.h>
#include <gdiplus.h>
#include "Texture.h"
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")

struct Edge
{
	Vertex v1, v2;
	Edge(Vertex p1, Vertex p2)
	{
		if (p1.Y() < p2.Y())
		{
			v1 = p1;
			v2 = p2;
		} else
		{
			v1 = p2;
			v2 = p1;
		}
	}
};

struct Span
{
	MyColor color1, color2;
	Vec2<float> texCoord1, texCoord2;
	int X1, X2;
	float depth1, depth2;
	Span(MyColor c1, int x1, float d1, MyColor c2, int x2, float d2, Vec2<float> t1, Vec2<float> t2)
	{
		if (x1 < x2)
		{
			X1 = x1;
			color1 = c1;
			X2 = x2;
			color2 = c2;
			depth1 = d1;
			depth2 = d2;
			texCoord1 = t1;
			texCoord2 = t2;
		} else
		{
			X2 = x1;
			color2 = c1;
			X1 = x2;
			color1 = c2;
			depth2 = d1;
			depth1 = d2;
			texCoord1 = t2;
			texCoord2 = t1;
		}
	}
};

class Rasterizer
{
public:
	int i = 0;
	int width, height;
	byte* data; //store pixel color value row major
	float* depth; //store depth value
	BITMAPINFO bmpi;
	Texture* texture;
	Rasterizer(int w, int h, Texture* tex = NULL) : width(w), height(h)
	{
		int size = width * height;
		data = new byte[size * 3];
		depth = new float[width * height];
		BITMAPINFOHEADER bmih;
		bmih.biBitCount = 24;
		bmih.biClrImportant = 0;
		bmih.biClrUsed = 0;
		bmih.biCompression = BI_RGB;
		bmih.biWidth = width;
		bmih.biHeight = -height;
		bmih.biPlanes = 1;
		bmih.biSize = 40;
		bmih.biSizeImage = width*height;
		bmpi.bmiHeader = bmih;
		texture = tex;
		Clear();
	}

	~Rasterizer()
	{
		delete[] data;
		delete[] depth;
	}

	void Rasterize(std::vector<Triangle> triangles, HDC hdc)
	{
		Clear();
		
		for (Triangle tri : triangles)
		{
			std::vector<Edge> edges;
			edges.push_back(Edge(tri.v1, tri.v2));
			edges.push_back(Edge(tri.v2, tri.v3));
			edges.push_back(Edge(tri.v3, tri.v1));
			int maxLength = 0;
			int longEdge = 0;
			for (int i = 0; i < 3; i++)
			{
				int length = edges[i].v2.Y() - edges[i].v1.Y();
				if (length > maxLength)
				{
					maxLength = length;
					longEdge = i;
				}
			}
			int shortEdge1 = (longEdge + 1) % 3;
			int shortEdge2 = (longEdge + 2) % 3;
			DrawSpansBetweenEdges(edges[longEdge], edges[shortEdge1], hdc);
			DrawSpansBetweenEdges(edges[longEdge], edges[shortEdge2], hdc);
		}
		SetDIBitsToDevice(hdc, 0, 0, width, height, 0, 0, 0, height, data, &bmpi, DIB_RGB_COLORS);
		i++;
	}

	void DrawSpansBetweenEdges(Edge e1, Edge e2, HDC hdc)
	{
		int e1Ydiff = (int)e1.v2.Y() - (int)e1.v1.Y();
		if (e1Ydiff == 0)
		{
			return;
		}
		int e2Ydiff = (int)e2.v2.Y() - (int)e2.v1.Y();
		if (e2Ydiff == 0) {
			return;
		}
		float e1Xdiff = e1.v2.X() - e1.v1.X();
		float e2Xdiff = e2.v2.X() - e2.v1.X();
		float e1Zdiff = e1.v2.Z() - e1.v1.Z();
		float e2Zdiff = e2.v2.Z() - e2.v1.Z();
		MyColor e1ColorDiff = e1.v2.color - e1.v1.color;
		MyColor e2ColorDiff = e2.v2.color - e2.v1.color;

		float factor1 = (e2.v1.Y() - e1.v1.Y())/e1Ydiff;
		float factorStep1 = 1.0f / e1Ydiff;
		float factor2 = 0.0f;
		float factorStep2 = 1.0f / e2Ydiff;
		Vec2<float> tex1Diff = e1.v2.texCoord - e1.v1.texCoord;
		Vec2<float> tex2Diff = e2.v2.texCoord - e2.v1.texCoord;
		for (int y = e2.v1.Y(); y < e2.v2.Y(); y++)
		{
			int x1 = e1.v1.X() + (int)(e1Xdiff * factor1);
			int x2 = e2.v1.X() + (int)(e2Xdiff * factor2);
			float depth1 = e1.v1.Z() + e1Zdiff * factor1;
			float depth2 = e2.v1.Z() + e2Zdiff * factor2;
			Vec2<float> t1(0,0);
			Vec2<float> t2(0,0);
			if (texture != NULL)
			{
				t1 = e1.v1.texCoord + tex1Diff*factor1;
				t2 = e2.v1.texCoord + tex2Diff*factor2;
			}
			Span span(e1.v1.color + (e1ColorDiff * factor1),
				x1,
				depth1,
				e2.v1.color + (e2ColorDiff * factor2),
				x2,
				depth2,
				t1,
				t2);
			DrawSpan(span, y, hdc);
			factor1 += factorStep1;
			factor2 += factorStep2;
		}
	}

	void DrawSpan(Span span, int y, HDC hdc)
	{
		int xDiff = span.X2 - span.X1;
		if (xDiff == 0)
		{
			return;
		}
		MyColor colorDiff = span.color2 - span.color1;
		float depthDiff = span.depth2 - span.depth1;
		float factor = 0.0f;
		float factorStep = 1.0f / xDiff;
		Vec2<float> texDiff = span.texCoord2 - span.texCoord1;
		for (int x = span.X1; x < span.X2; x++)
		{
			MyColor color = span.color1 + (colorDiff * factor);

			float pixelDepth = span.depth1 + (depthDiff * factor);
			if (x > 0 && x < 640)
			{
				if (pixelDepth < depth[640 * y + x]) {
					if (texture != NULL && texDiff != 0)
					{
						Vec2<float> texCoord = span.texCoord1 + texDiff*factor;
						MyColor cTex = texture->GetPixel(texCoord[0], texCoord[1]);
						data[640 * y * 3 + x * 3] = cTex.R;
						data[640 * y * 3 + x * 3 + 1] = cTex.G;
						data[640 * y * 3 + x * 3 + 2] = cTex.B;
						depth[640 * y + x] = pixelDepth;
					}
					else 
					{
						data[640 * y * 3 + x * 3] = color.R;
						data[640 * y * 3 + x * 3 + 1] = color.G;
						data[640 * y * 3 + x * 3 + 2] = color.B;
						depth[640 * y + x] = pixelDepth;
					}
				}
			}
			factor += factorStep;
		}
	}

	void Clear()
	{
		int size = width * height;
		for (int i = 0; i < size; ++i)
		{
			depth[i] = 1;
		}
		for (int i = 0; i < size*3; i+=3)
		{
			data[i] = 0;
			data[i+1] = 0;
			data[i+2] = 0;
		}
	}
};
