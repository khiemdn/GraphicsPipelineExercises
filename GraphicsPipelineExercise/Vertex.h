// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Vertex.h" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Vertex.h
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma once
#include "Matrix.h"
#include "Vector.h"

struct MyColor
{
	int R, G, B;
	MyColor() : R(0), G(0), B(0) {}
	MyColor(int r, int g, int b): R(r), G(g), B(b){}
	MyColor operator+(const MyColor& other)
	{
		return MyColor(R + other.R, G + other.G, B + other.B);
	}
	MyColor operator-(const MyColor& other)
	{
		return MyColor(R - other.R, G - other.G, B - other.B);
	}
	MyColor operator*(float factor)
	{
		return MyColor(R*factor, G*factor, B*factor);
	}
};

struct Vertex
{
	Matrix4x1<float> matrix;
	Vec2<float> texCoord;
	MyColor color;
	Vertex(float x, float y, float z, MyColor c = MyColor()): matrix(Matrix4x1<float>()), texCoord(Vec2<float>(0, 0))
	{
		matrix[0][0] = x;
		matrix[1][0] = y;
		matrix[2][0] = z;
		matrix[3][0] = 1;
		color = c;
	}

	Vertex() : matrix(Matrix4x1<float>()), texCoord(Vec2<float>(0,0))
	{
	}

	~Vertex()
	{
	}

	Vec3<float> operator-(const Vertex& other) const
	{
		Vec3<float> result;
		result[0] = matrix[0][0] - other.matrix[0][0];
		result[1] = matrix[1][0] - other.matrix[1][0];
		result[2] = matrix[2][0] - other.matrix[2][0];
		return result;
	}

	void ApplyTransformMatrix(Matrix4x4<float> m)
	{
		Matrix4x1<float> result = m * matrix;
		matrix = result;
	}

	float X()
	{
		return matrix[0][0];
	}

	float Y()
	{
		return matrix[1][0];
	}

	float Z()
	{
		return matrix[2][0];
	}

	Vertex PixelFormat()
	{
		matrix[0][0] = (int)matrix[0][0];
		matrix[1][0] = (int)matrix[1][0];
		return *this;
	}

	void SetTextureCoord(float x, float y)
	{
		texCoord[0] = x;
		texCoord[1] = y;
	}
};
