#pragma once
#include "Vertex.h"
#include "Utils.h"
#include <windows.h>
#include <vector>
#include "Rasterizer.h"

class Cube
{
public:
	std::vector<Vertex> vlist = {};
	std::vector<Matrix4x4<float>> transformList = {};
	float width, length, height;
	Cube(float w, float h, float l) : width(w), length(l), height(h)
	{
		vlist.push_back(Vertex(-w / 2, -h / 2, l / 2, MyColor(255, 0, 0)));
		vlist.push_back(Vertex(-w / 2, -h / 2, -l / 2, MyColor(0, 255, 0)));
		vlist.push_back(Vertex(w / 2, -h / 2, -l / 2, MyColor(0, 0, 255)));
		vlist.push_back(Vertex(w / 2, -h / 2, l / 2, MyColor(255, 255, 0)));
		vlist.push_back(Vertex(-w / 2, h / 2, l / 2, MyColor(0, 255, 255)));
		vlist.push_back(Vertex(-w / 2, h / 2, -l / 2, MyColor(255, 0, 255)));
		vlist.push_back(Vertex(w / 2, h / 2, -l / 2, MyColor(255, 255, 255)));
		vlist.push_back(Vertex(w / 2, h / 2, l / 2, MyColor(0, 0, 0)));
	}
	~Cube() {}

	void ApplyTransformMatrix(Matrix4x4<float> m)
	{
		transformList.push_back(m);
	}

	void ClearTransform()
	{
		transformList.clear();
	}

	void DrawEdges(HDC hdc)
	{
		std::vector<Vertex> vDraw(vlist.begin(), vlist.end());
		for (auto m = transformList.cbegin(); m != transformList.cend(); ++m) {
			for (auto i = vDraw.begin(); i != vDraw.end(); ++i)
			{
				i->ApplyTransformMatrix(*m);
			}
		}

		DrawLine(hdc, vDraw[0], vDraw[1]);
		DrawLine(hdc, vDraw[1], vDraw[2]);
		DrawLine(hdc, vDraw[2], vDraw[3]);
		DrawLine(hdc, vDraw[3], vDraw[0]);

		DrawLine(hdc, vDraw[4], vDraw[5]);
		DrawLine(hdc, vDraw[5], vDraw[6]);
		DrawLine(hdc, vDraw[6], vDraw[7]);
		DrawLine(hdc, vDraw[7], vDraw[4]);

		DrawLine(hdc, vDraw[0], vDraw[4]);
		DrawLine(hdc, vDraw[1], vDraw[5]);
		DrawLine(hdc, vDraw[2], vDraw[6]);
		DrawLine(hdc, vDraw[3], vDraw[7]);
	}

	std::vector<Triangle> GetVerticesData()
	{
		std::vector<Vertex> vDraw(vlist.begin(), vlist.end());
		for (auto m = transformList.cbegin(); m != transformList.cend(); ++m) {
			for (auto i = vDraw.begin(); i != vDraw.end(); ++i)
			{
				i->ApplyTransformMatrix(*m);
			}
		}
		std::vector<Triangle> triangles = {};

		triangles.push_back(Triangle(vDraw[0].PixelFormat(), vDraw[1].PixelFormat(), vDraw[2].PixelFormat()));
		triangles.push_back(Triangle(vDraw[2].PixelFormat(), vDraw[3].PixelFormat(), vDraw[0].PixelFormat()));

		triangles.push_back(Triangle(vDraw[4].PixelFormat(), vDraw[5].PixelFormat(), vDraw[6].PixelFormat()));
		triangles.push_back(Triangle(vDraw[6].PixelFormat(), vDraw[7].PixelFormat(), vDraw[4].PixelFormat()));

		triangles.push_back(Triangle(vDraw[0].PixelFormat(), vDraw[1].PixelFormat(), vDraw[5].PixelFormat()));
		triangles.push_back(Triangle(vDraw[5].PixelFormat(), vDraw[4].PixelFormat(), vDraw[0].PixelFormat()));

		triangles.push_back(Triangle(vDraw[2].PixelFormat(), vDraw[3].PixelFormat(), vDraw[7].PixelFormat()));
		triangles.push_back(Triangle(vDraw[6].PixelFormat(), vDraw[7].PixelFormat(), vDraw[2].PixelFormat()));


		vDraw[0].SetTextureCoord(0, 0);
		vDraw[4].SetTextureCoord(0, 1);
		vDraw[7].SetTextureCoord(1, 1);
		vDraw[3].SetTextureCoord(1, 0);

		triangles.push_back(Triangle(vDraw[0].PixelFormat(), vDraw[3].PixelFormat(), vDraw[7].PixelFormat()));
		triangles.push_back(Triangle(vDraw[4].PixelFormat(), vDraw[7].PixelFormat(), vDraw[0].PixelFormat()));

		triangles.push_back(Triangle(vDraw[1].PixelFormat(), vDraw[2].PixelFormat(), vDraw[5].PixelFormat()));
		triangles.push_back(Triangle(vDraw[5].PixelFormat(), vDraw[6].PixelFormat(), vDraw[2].PixelFormat()));

		return triangles;
	}
};

