#pragma once
#include "Matrix.h"
#include "Vertex.h"
#include "Vector.h"

class Camera
{
public:
	Matrix4x4<float> LookAt(Vertex position, Vertex target, Vec3<float> Up)
	{
		Vec3<float> direction = target - position;
		direction.normalize();
		
		Matrix4x4<float> result = CreateTranslation(-position.matrix[0][0], -position.matrix[1][0], -position.matrix[2][0]);
		return result;
	}
};
