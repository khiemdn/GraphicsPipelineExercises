// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Utils.h" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Utils.h
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma once
#pragma once
#include <windows.h>
#include "Vertex.h"
#include <vector>
#include <fstream>

float LerpResult(float start, float end, float ratio)
{
	return start + ratio * (end - start);
}

void DrawLine(HDC hdc, Vertex p1, Vertex p2)
{
	float startX = p1.matrix[0][0];
	float startY = p1.matrix[1][0];
	float endX = p2.matrix[0][0];
	float endY = p2.matrix[1][0];

	bool swapXY = abs(endX - startX) < abs(endY - startY);

	if (swapXY)
	{
		std::swap(startX, startY);
		std::swap(endX, endY);
	}

	//	if (abs(endX - startX) > abs(endY - startY))
	//	{
	for (int x = (int)startX; x != (int)endX;)
	{
		float ratio = (x - startX) / (endX - startX);
		float y = LerpResult(startY, endY, ratio);
		int red = LerpResult(p1.color.R, p2.color.R, ratio);
		int green = LerpResult(p1.color.G, p2.color.G, ratio);
		int blue = LerpResult(p1.color.B, p2.color.B, ratio);
		if (swapXY)
		{
			SetPixel(hdc, y, x, RGB(red, green, blue));
		}
		else
		{
			SetPixel(hdc, x, y, RGB(red, green, blue));
		}
		startX < endX ? x++ : x--;
	}
	//	}
	//	else
	//	{
	//		for (int y = (int)startY; y != (int)endY;)
	//		{
	//			float ratio = (y - startY) / (endY - startY);
	//			float newX = LerpResult(startX, endX, ratio);
	//			int red = LerpResult(p1.color.R, p2.color.R, ratio);
	//			int green = LerpResult(p1.color.G, p2.color.G, ratio);
	//			int blue = LerpResult(p1.color.B, p2.color.B, ratio);
	//			SetPixel(hdc, newX, y, RGB(red, green, blue));
	//			startY < endY ? y++ : y--;
	//		}
	//	}
}
