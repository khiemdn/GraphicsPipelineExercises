// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Matrix.h" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Matrix.h
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma once
#include <math.h>
#include "Vector.h"

#define PI 3.14159265359

template <typename T>
struct Matrix4x1
{
	Vec1<T> value[4];

	Matrix4x1()
	{
		for (int i = 0; i < 4; i++)
		{
			value[i][0] = 0;
		}
	}

	Vec1<T>& operator[](int r)
	{
		return value[r];
	}

	const Vec1<T>& operator[](int r) const
	{
		return value[r];
	}
};

template <typename T>
struct Matrix4x4
{
	Vec4<T> value[4];

	Matrix4x4()
	{
		for (int i = 0; i < 4; i++)
		{
			value[i][0] = 0;
			value[i][1] = 0;
			value[i][2] = 0;
			value[i][3] = 0;
		}
	}

	Matrix4x4(T m[][4])
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				value[i][j] = m[i][j];
			}
		}
	}

	Vec4<T>& operator[](int r)
	{
		return value[r];
	}

	const Vec4<T>& operator[](int r) const
	{
		return value[r];
	}

	Matrix4x4<T> operator*(const Matrix4x4<T>& r)
	{
		Matrix4x4<T> result;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				T v = 0;
				for (int k = 0; k < 4; k++)
				{
					v += value[i][k] * r[k][j];
				}
				result[i][j] = v;
			}
		}
		return result;
	}

	Matrix4x1<T> operator*(const Matrix4x1<T>& other)
	{
		Matrix4x1<T> result;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 1; j++)
			{
				T v = 0;
				for (int k = 0; k < 4; k++)
				{
					v += value[i][k] * other[k][j];
				}
				result[i][j] = v;
			}
		}
		return result;
	}
};

Matrix4x4<float> CreateTranslation(float x = 0, float y = 0, float z = 0)
{
	float transMatrix[4][4] = {
		{1, 0, 0, x},
		{0, 1, 0, y},
		{0, 0, 1, z},
		{0, 0, 0, 1}
	};
	Matrix4x4<float> result(transMatrix);
	return result;
}

Matrix4x4<float> CreateScale(float xScale = 1, float yScale = 1, float zScale = 1)
{
	float scaleMatrix[4][4] = {
		{xScale, 0, 0, 0},
		{0, yScale, 0, 0},
		{0, 0, zScale, 0},
		{0, 0, 0, 1}
	};
	Matrix4x4<float> result(scaleMatrix);
	return result;
}

Matrix4x4<float> CreateXRotation(float xDegrees)
{
	float rad = xDegrees * PI / 180.0;
	float rotXMatrix[4][4] = {
		{1, 0, 0, 0},
		{0, cosf(rad) , -sinf(rad), 0},
		{0, sinf(rad), cosf(rad), 0},
		{0, 0, 0, 1}
	};
	Matrix4x4<float> result(rotXMatrix);
	return result;
}

Matrix4x4<float> CreateYRotation(float yDegrees)
{
	float rad = yDegrees * PI / 180.0;
	float rotYMatrix[4][4] = {
		{cosf(rad), 0, sinf(rad), 0},
		{0, 1 , 0, 0},
		{-sinf(rad), 0, cosf(rad), 0},
		{0, 0, 0, 1}
	};
	Matrix4x4<float> result(rotYMatrix);
	return result;
}

Matrix4x4<float> CreateZRotation(float zDegrees)
{
	float rad = zDegrees * PI / 180.0;
	float rotZMatrix[4][4] = {
		{cosf(rad), -sinf(rad), 0, 0},
		{sinf(rad), cosf(rad), 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
	};
	Matrix4x4<float> result(rotZMatrix);
	return result;
}

Matrix4x4<float> OrthographicMatrix(float l, float r, float t, float b, float n, float f)
{
	Matrix4x4<float> m;
	m[0][0] = 2.0f / (t - b);
	m[1][1] = 2.0f / (r - l);
	m[2][2] = -2.0f / (f - n);
	m[3][3] = 1.0f;
	m[0][3] = -(r + l) / (r - l);
	m[1][3] = -(t + b) / (t - b);
	m[2][3] = -(f + n) / (f - n);
	return m;
}

Matrix4x4<float> ViewportTransform(int x, int y, int w, int h, int n, int f)
{
	float m[4][4] = {
		{ w / 2.0f, 0, 0, w / 2.0f },
		{ 0, -h / 2.0f, 0, h / 2.0f },
		{ 0, 0, (f - n) / 2.0f, (f + n) / 2.0f },
		{ 0, 0, 0, 1 }
	};
	Matrix4x4<float> result(m);
	return result;
}
